using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tests.Services;

namespace tests
{
    public class HomeController : Controller
    {

        [Route("get")]
        public async Task<int> Get(int num)
        {
            return await ArrayService.Get(num);
        }

        
        [Route("post")]
        public async Task<JsonResult> Post(int num)
        {
            await ArrayService.Add(num);
            var s = new JsonResult("post number is " + num);
            return s;
        }
        
    }
}
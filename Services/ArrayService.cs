using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;

namespace tests.Services
{
    public class ArrayService
    {
        static ArrayService()
        {
            _eventAsObservable = Observable.FromEventPattern(
                ev => SimpleEvent += ev,
                ev => SimpleEvent -= ev);
        }

        private static event EventHandler SimpleEvent;
        private static IObservable<EventPattern<object>> _eventAsObservable;
        private static Dictionary<int, int> _dictionary = new Dictionary<int, int>();


        public static async Task<int> Add(int num)
        {
            //System.Threading.Thread.Sleep(num * 1000);
            _dictionary.Add(num, num);
            SimpleEvent.Invoke(new object(), new Data {number = num});
            return 0;
        }

        public static async Task<int> Get(int num)
        {
            if (_dictionary.ContainsKey(num))
            {
                return _dictionary[num];
            }
            else
            {
                TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
                
                _eventAsObservable.Subscribe(number =>
                {
                    var curNum = ((Data) number.EventArgs).number;
                    if (curNum == num)
                        tcs.SetResult(curNum);
                });
                return await tcs.Task;
            }
        }
    }

    public class Data : EventArgs
    {
        public int number { get; set; }
    }
}